/**
 * @file planner_a_star.h
 * @brief Describes the PlannerAStar Class
 * @author Mario Garzon
 * @date 12/03/2013.
 *
 */

/*  Based on the A* Algorithm Implementation using STL
 *  by: Copyright (C)2001-2005 Justin Heyes-Jones
 *
 *  	Permission is given by the author to freely redistribute and
 * 		include this code in any program as long as this credit is
 *		given where due.
 *
 */

#ifndef PLANNERASTAR_H_
#define PLANNERASTAR_H_


#include <assert.h>
#include <stdio.h>
#include <nav_msgs/OccupancyGrid.h>

// stl includes
#include <vector>
#include <planner_upm/node.h>
#include <planner_upm/planner_tools.h>

#define SQR2 1.414213562
// disable warning that debugging information has lines that are truncated
// occurs in stl headers
//#pragma warning( disable : 4786 )

/**
 * @brief Implements the A Start Planner
 *
 */
class PlannerAStar : public PlannerTools
{
	public:
	/**
	 * @brief constructor just initializes private data
	 */
	PlannerAStar(nav_msgs::OccupancyGrid map_);

	virtual ~PlannerAStar();
	// Advances search one step
	/**
	 * @brief Main function. Performs one search step.
	 * @return The current state of the search
	 */
	unsigned int SearchStep();
	/**
	 * @brief Sets starting and goal position for the planning
	 * @param Start Starting node for the planning
	 * @param Goal Goal node for the plannig
	 */
	void SetStartAndGoalStates( Node &Start, Node &Goal );

	/**
	 * @brief Finds the successors of an open node
	 * @param node  the current node to be explored
	 * @param parent_node The parent node (so it doesn't go backwards)
	 * @return True if it finds at least one successor, false otherwise.
	 */
	bool GetSuccessors(Node *node, Node *parent_node );

	private:

};

#endif /* PLANNERASTAR_H_ */
