/**
 * @file node.h
 * @brief Describes the Node Class for the Planning
 * @author Mario Garzon
 * @date 12/03/2013.
 *
 */

#ifndef NODE_H_
#define NODE_H_

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

/**
 * @brief Simple Node Class
 * Basic node, used for the Planning.
 */
class Node {

public:

		/**
		 * @brief Constructor of the class
		 */
		Node();
		/**
		 * @brief Constructor based on another node
		 * @param nodeInit Node to be used as base to construct.
		 */
		Node(Node &nodeInit);

		/**
		 * @brief Constructor based on a given geometry_msgs::Pose
		 * @param nodePose Pose to be used as base to construct.
		 */
        Node(geometry_msgs::Pose nodePose);
        /**
         * @brief Constructor based on a given position x,y
         * @param px position in X to initialize the node
         * @param py position in Y to initialize the node
         */
        Node(double px, double py);

        /**
         * @brief Computes the estimated Euclidean distance to the goal
         * @deprecated Use the specific type of distance instead (e.g. EuclideanDistance())
         * @param nodeGoal The goal node to compute the distance with
         * @return The distance to the given goal node.
         */
        double GoalDistanceEstimate( Node &nodeGoal );
        /**
         * @brief Computes the Manhattan distance to the goal
         * @param nodeGoal The goal node to compute the distance with
         * @return The distance to the given goal node.
         */
        double ManhattanDistance( Node &nodeGoal );
        /**
         * @brief Computes the Chebyshev distance to the goal
         * @param nodeGoal The goal node to compute the distance with
         * @return The distance to the given goal node.
         */
        double ChebyshevDistance( Node &nodeGoal );
        /**
         * @brief Computes the Euclidean Distance Square to the goal
         * @param nodeGoal The goal node to compute the distance with
         * @return The distance to the given goal node.
         */
        double EuclideanDistanceSquare( Node &nodeGoal );
        /**
         * @brief Computes the Euclidean Distance to the goal
         * @param nodeGoal The goal node to compute the distance with
         * @return The distance to the given goal node.
         */
        double EuclideanDistance( Node &nodeGoal );

        /**
         * @brief Checks whether the node is the goal.
         * @deprecated Use IsSameState() instead.
         * @param nodeGoal the goal to compare with.
         * @return True if it is the same, False otherwise.
         */
		bool IsGoal( Node &nodeGoal );

        /**
         * @brief Checks whether the node is the same with other.
         * @param nodeGoal the node to compare with.
         * @return True if it is the same, False otherwise.
         */
		bool IsSameState( Node &nodeGoal );

		/**
		 * @brief Prints the basic info of the node
		 */
		void PrintNodeInfo();
		/**
		 * @brief descructor
		 */
        virtual ~Node();

        double getPosX() const {
        	return nodePose_.position.x;
        }
        double getPosY() const {
        	return nodePose_.position.y;
        }

        /**
         * Sets the position of the node from X and Y values
         * @param px New X position of the node
         * @param py New Y position of the node
         */
        void setPosefromXY(double px, double py);

        double get_f_score() const {
        	return f_score;
        }

		void set_f_score(double score) {
			f_score = score;
		}

		double get_g_score() const {
			return g_score;
		}

		void set_g_score(double score) {
			g_score = score;
		}

		double get_h_score() const {
			return h_score;
		}

		void set_h_score(double score) {
			h_score = score;
		}

    	// current pose
        geometry_msgs::Pose nodePose_;
        double g_score; 		//! the past path-cost function, which is the known distance from the starting node to the current node
		double h_score; 		//! "heuristic estimate" of the distance from x to the goal
		double f_score; 		//! knowledge-plus-heuristic cost function of node x. f_score = g_score + h_score
		double cost_value;		//! Cost value from the map

		bool visited;			//! Marks whether a node has been visited or not.

		Node *parentNode; 	//! used during the search to record the parent of successor nodes
		Node *childNode; 	//! used after the search for the application to view the search in reverse

		ros::Time nodeTimestamp;

};
/**
 * @brief Used to make a heap of a nodes vector with f_score
 */
class HeapCompare_f
	{
		public:

			/**
			 * @brief Compares the f_score of two nodes
			 * @param x The first node
			 * @param y The second node
			 * @return true if f_score in x is larger than f_score in y, false otherwise.
			 */
			bool operator() ( const Node *x, const Node *y ) const
			{
				return x->get_f_score() > y->get_f_score();
			}
	};
/**
 * @brief Used to make a heap of a nodes vector with g_score
 */
class HeapCompare_g
	{
		public:
			/**
			 * @brief Compares the f_score of two nodes
			 * @param x The first node
			 * @param y The second node
			 * @return true if f_score in x is larger than f_score in y, false otherwise.
			 */
			bool operator() ( const Node *x, const Node *y ) const
			{
				return x->get_g_score() > y->get_g_score();
			}
	};


#endif /* NODE_H_ */
