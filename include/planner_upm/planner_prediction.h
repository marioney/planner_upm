/**
 * @file planner_prediction.h
 * @brief Implements the PlannerForPred Class, it controls the planning
 * @author Mario Garzon
 * @date 08/03/2013.
 *
 */
/*  Based on the A* Algorithm Implementation using STL
 *  by: Copyright (C)2001-2005 Justin Heyes-Jones
 *
 *  	Permission is given by the author to freely redistribute and
 * 		include this code in any program as long as this credit is
 *		given where due.
 *
 */

#ifndef PLANNER_H_
#define PLANNER_H_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <planner_upm/planner_a_star.h>
#include <planner_upm/planner_dijkstra.h>
#include <planner_upm/planner_tools.h>
#include <planner_upm/node.h>

class PlannerForPred
{
	public:
	PlannerForPred(tf::TransformListener& tf);
	virtual ~PlannerForPred();

	void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& mapMsg);
	void initPoseCallback(const geometry_msgs::PoseWithCovarianceStampedPtr& initPoseMsg);
	void goalPoseCallback(const geometry_msgs::PoseStampedConstPtr& goalPoseMsg);
	void getPlanAStar(const geometry_msgs::PoseStamped& initPose, const geometry_msgs::PoseStamped& goalPose);
	void getPlanAStar(const unsigned int posX_init, const unsigned int posY_init, const unsigned int posX_goal, const unsigned int posY_goal);
	void getPlanAStar();
	void getPlanDijkstra(const geometry_msgs::PoseStamped& initPose, const geometry_msgs::PoseStamped& goalPose);
	void getPlanDijkstra(const unsigned int posX_init, const unsigned int posY_init, const unsigned int posX_goal, const unsigned int posY_goal);
	void getPlanDijkstra();
	void getTwoPlans();
	bool checkPositonInMap(unsigned int& posX, unsigned int& posY);
	bool checkPositonInMap(unsigned int& posX, unsigned int& posY, unsigned int MAX_SIZE_X, unsigned int MAX_SIZE_Y);
	void worldToMap(double wx, double wy, unsigned int& mx, unsigned int& my) const;
	void worldToMap(const geometry_msgs::PoseStamped& Pose, unsigned int& mx, unsigned int& my) const;
	void mapToWorld(unsigned int mx, unsigned int my, double& wx, double& wy) const;
	void spin(const ros::TimerEvent& e);

	/**
	 * ROS Publishers and subscribers.
	 */

	ros::Subscriber mapSubscriber;	// Subscriber for the map topic (OccupancyGrid);
	ros::Subscriber initPoseSubscriber;
	ros::Subscriber goalPoseSubscriber;
	ros::Publisher pathPublisherAstar;	// Publisher for the obtained path.
	ros::Publisher pathPublisherDijkstra;	// Publisher for the obtained path.
	ros::Publisher initPosePublisher;	// Publisher for the obtained path.
	ros::Publisher goalPosePublisher;	// Publisher for the obtained path.


	ros::Timer timer_;
	nav_msgs::OccupancyGrid GlobalMap_;
	nav_msgs::Path plannedPath;
	geometry_msgs::PoseStamped pose_at_x;
	geometry_msgs::PoseStamped currentPose;

	//we need this to be able to initialize the map using a latched topic approach
	//strictly speaking, we don't need the lock, but since this all happens on startup
	//and there is little overhead... we'll be careful and use it anyways just in case
	//the compiler or scheduler does something weird with the code
	boost::recursive_mutex map_data_lock_;
	nav_msgs::MapMetaData map_meta_data_;

	bool map_initialized_; 		// Controls the initialization of the map.
	ros::Timer loopTimer;
	//PlannerAStar *myAStarSearch;



};

#endif /* PLANNER_H_ */
