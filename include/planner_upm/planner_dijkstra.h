/**
 * @file planner_dijkstra.h
 * @brief Describes the PlannerDijkstra Class
 * @author Mario Garzon
 * @date 20/03/2013.
 *
 */
/*  Based on the A* Algorithm Implementation using STL
 *  by: Copyright (C)2001-2005 Justin Heyes-Jones
 *
 *  	Permission is given by the author to freely redistribute and
 * 		include this code in any program as long as this credit is
 *		given where due.
 *
 */
#ifndef PLANNERDIJKSTRA_H_
#define PLANNERDIJKSTRA_H_

#include <assert.h>
#include <stdio.h>
#include <nav_msgs/OccupancyGrid.h>

// stl includes
#include <vector>
#include <planner_upm/node.h>
#include <planner_upm/planner_tools.h>

#define SQR2 1.414213562
/**
 * @brief Implements the Dijkstra Planner
 *
 */
class PlannerDijkstra : public PlannerTools {
public:
	/**
	 * @brief constructor just initializes private data
	 */
	PlannerDijkstra(nav_msgs::OccupancyGrid map_);
	virtual ~PlannerDijkstra();
	/**
	 * @brief Sets starting and goal position for the planning
	 * @param Start Starting node for the planning
	 * @param Goal Goal node for the plannig
	 */
	void SetStartAndGoalStates( Node &Start, Node &Goal );

	/**
	 * @brief Main function. Performs one search step.
	 * @return The current state of the search
	 */
	unsigned int SearchStep();
	/**
	 * @brief Finds the successors of an open node
	 * @param node  the current node to be explored
	 * @param parent_node The parent node (so it doesn't go backwards)
	 * @return True if it finds at least one successor, false otherwise.
	 */
	bool GetSuccessors(Node *node, Node *parent_node );


	std::vector< Node * > m_neighbors; //! Possible neighbors from the closed list.


	Node *GetNeighborListNext();
	Node *GetNeighborListStart();
};

#endif /* PLANNERDIJKSTRA_H_ */
