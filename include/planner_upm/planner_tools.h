/**
 * @file planner_tools.h
 * @brief Describes the PlannerTools Class, a base for the Planners.
 * @author Mario Garzon.
 * @date 14/04/2014.
 *
 * 	It defines a set of basic and common tools used for planning
 * 	algorithms.
 *
 */
/*  Based on the A* Algorithm Implementation using STL
 *  by: Copyright (C)2001-2005 Justin Heyes-Jones
 *
 *  	Permission is given by the author to freely redistribute and
 * 		include this code in any program as long as this credit is
 *		given where due.
 *
 */
#ifndef PLANNER_TOOLS_H_
#define PLANNER_TOOLS_H_

#include <planner_upm/node.h>
#include <assert.h>
#include <stdio.h>
#include <exception>


//ROS Includes
#include <nav_msgs/OccupancyGrid.h>

// stl includes
#include <vector>

#define SQR2 1.414213562  //Root Square of Two.. to avoid computing it several times,
/**
 * @brief Base Class for the Planners.
 *  It implements a series of functions and common utilities
 *  used for the planner classes. The planning works over an occupancy grid which is the
 *  cost map used for the planning.
 */
class PlannerTools {
public:
	/**
	 * @brief Constructor of the class
	 * @param map_ the cost map used for the planning.
	 */
	PlannerTools(nav_msgs::OccupancyGrid map_);
	virtual ~PlannerTools();

	/**
	 * Enumerator for the possible statuses of the search.
	 */
	enum
		{
			SEARCH_STATE_NOT_INITIALISED,
			SEARCH_STATE_SEARCHING,
			SEARCH_STATE_SUCCEEDED,
			SEARCH_STATE_FAILED,
			SEARCH_STATE_OUT_OF_MEMORY,
			SEARCH_STATE_INVALID
		};

		/**
		 * @brief Cancels the search and frees up all the memory
		 */
		void CancelSearch()
		{
			m_CancelRequest = true;
		}
//		/*
//		 * @brief Sets Start and Goal States
//		 * @param Start The starting node
//		 * @param Goal The goal node
//		 */
		//void SetStartAndGoalStates( Node &Start, Node &Goal );
		/**
		 * @brief Performs one step in the search
		 */
		unsigned int SearchStep();
		/**
		 * @brief Adds the one node to the successor nodes list, used to expand the search frontier
		 * @param Successor the node to be added to the successors list.
		 */
		bool AddSuccessor(Node &Successor);
		/**
		 * @brief Frees the solution nodes.
		 * 		  Used to clean up all the used Node memory when the search is finished.
		 */
		void FreeSolutionNodes();
		/**
		 * @brief Ensures that all the memory has been Freed.
		 */
		void EnsureMemoryFreed(){ FreeAllNodes(); };
		/**
		 * @brief Gets the start node of the solution
		 */
		Node *GetSolutionStart();
		/**
		 * @brief Gets the next node of the solution
		 */
		Node *GetSolutionNext();
		/**
		 * @brief Gets the last node of the solution
		 */
		Node *GetSolutionEnd();
		/**
		 * @brief Gets the previous node of the solution
		 */
		Node *GetSolutionPrev();
		/**
		 * @brief Gets the start of he open nodes list.
		 * @return Pointer to the first node on the open nodes list.
		 */
		Node *GetOpenListStart();
		/**
		 * @brief Gets the start of the open nodes list.
		 * @param f used to receive the f score of the node
		 * @param g used to receive the g score of the node
		 * @param h used to receive the h score of the node
		 * @return Pointer to the first node on the open nodes list.
		 */
		Node *GetOpenListStart( double &f, double &g, double &h );
		/**
		 * @brief Gets the next node of the open nodes list.
		 */
		Node *GetOpenListNext();
		/**
		 * @brief Gets  the next node of the open nodes list.
		 * @param f used to receive the f score of the node
		 * @param g used to receive the g score of the node
		 * @param h used to receive the h score of the node
		 * @return Pointer to the next node on the open nodes list.
		 */
		Node *GetOpenListNext( double &f, double &g, double &h );
		/**
		 * @brief Gets the start of he closed nodes list.
		 * @return Pointer to the first node on the closed nodes list.
		 */
		Node *GetClosedListStart();
		/**
		 * @brief Gets the start of the closed nodes list.
		 * @param f used to receive the f score of the node
		 * @param g used to receive the g score of the node
		 * @param h used to receive the h score of the node
		 * @return Pointer to the first node on the closed nodes list.
		 */
		Node *GetClosedListStart( double &f, double &g, double &h );
		/**
		 * @brief Gets the next node of the closed nodes list.
		 */
		Node *GetClosedListNext();
		/**
		 * @brief Gets  the next node of the closed nodes list.
		 * @param f used to receive the f score of the node
		 * @param g used to receive the g score of the node
		 * @param h used to receive the h score of the node
		 * @return Pointer to the next node on the closed nodes list.
		 */
		Node *GetClosedListNext( double &f, double &g, double &h );

//		bool GetSuccessors(Node *node, Node *parent_node );

		/**
		 * @brief Gets the cost of a node in the map
		 * @param node the node to be evaluated in the map
		 * @returns The cost value of the node in the map
		 */
		int getMapValue(const Node &node);
		/**
		 * @brief Gets the cost of a node in the map
		 * @param pos_x the position x of the map to be evaluated.
		 * @param pos_y the position y of the map to be evaluated.
		 * @returns The cost value of the node in the map
		 */
		int getMapValue(const unsigned int pos_x, const unsigned int pos_y);

		/**
		 * @brief Checks whether a position is on the map
		 * @param node the node to be evaluated.
		 * @returns true if it is on the map, false otherwise.
		 */
		bool isNodeInMap(Node &node);
		/**
		 * @brief Checks whether a position is on the map
		 * @param pos_x the position x of the map to be evaluated.
		 * @param pos_y the position y of the map to be evaluated.
		 * @returns true if it is on the map, false otherwise.
		 */
		bool isNodeInMap(int pos_x, int pos_y);

		/**
		 * @brief Gets the number of steps in the search.
		 */
		int GetStepCount() { return getSteps(); }

		int getSteps() const {
		return m_Steps;
		}


		int MaxDir;
		int MapThreshold;
		static int deltaX[];
		static int deltaY[];


		/**
		 * @brief Frees all the memory
		 * 		This is called when a search fails or is cancelled to free all used memory
		 */
		void FreeAllNodes();
		/**
		 * @brief Frees the unused nodes memory
		 * 		This call is made by the search class when the search ends. A lot of nodes may be
		 * 		created that are still present when the search ends. They will be deleted by this
		 * 		routine once the search ends.
		 */
		void FreeUnusedNodes();
		/**
		 * @brief Frees the unused nodes in a given vector
		 * @param list The vector of nodes to be processed.
		 */
		void FreeUnusedNodesInVector(std::vector< Node * > list);
		/**
		 * @brief Frees all the nodes in a given vector
		 * @param node_list The vector of nodes to be processed.
		 */
		void FreeNodesInVector(std::vector< Node * > node_list);
		/**
		 * @brief Frees all the nodes or only the ones not been used
		 * @param node_list The vector of nodes to be processed.
		 * @param keep_used Whether to keep or not the used nodes
		 */
		void FreeNodesInVector(std::vector< Node * > node_list, const bool keep_used);

		/**
		 * The global cost map.
		 */
		nav_msgs::OccupancyGrid costMap_;

		/**
		 * The list of open nodes.
		 * It is a simple vector but used as a heap, cf. Steve Rabin's game gems article)
		 */
		std::vector < Node *> m_OpenList;

		/**
		 * The list of closed nodes.
		 * It is a simple vector.
		 */
		std::vector< Node * > m_ClosedList;

		/**
		 * The list of possible succesors
		 * Successors is a vector filled out by the user each type successors to a node
		 * are generated
		 */
		std::vector< Node * > m_Successors;

		/**
		 * The state of the search
		 */
		unsigned int m_State;

		/**
		 * The number of steps in the search
		 */
		int m_Steps;

		/**
		 * Pointer to the Start position node
		 */
		Node *m_Start;
		/**
		 * Pointer to the Goal position node
		 */
		Node *m_Goal;
		/**
		 * Used to extract the solution
		 */
		Node *m_CurrentSolutionNode;

		//Debug : need to keep these two iterators around
		// for the user Dbg functions
		typename std::vector< Node * >::iterator iterDbgOpen;
		typename std::vector< Node * >::iterator iterDbgClosed;

		// debugging : count memory allocation and free's
		int m_AllocateNodeCount;
		/**
		 * Used to cancel the search
		 */
		bool m_CancelRequest;
};

#endif /* PLANNER_TOOLS_H_ */
