/*
 * Planner.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: garzonov
 */

#include <planner_upm/planner_prediction.h>

PlannerForPred::PlannerForPred(tf::TransformListener& tf):
map_initialized_(false)
{
	// TODO Auto-generated constructor stub
	ros::NodeHandle private_nh("~");

	std::string map_topic;
	std::string Path_topic_Astar;
	std::string Path_topic_Dijkstra;
	std::string Init_Pose_topic;
	std::string Goal_Pose_topic;

	double freq;

    private_nh.param("map_topic", map_topic, std::string("/Global_Cost"));
    private_nh.param("Path_topic_Astar", Path_topic_Astar, std::string("/Plan_AStar"));
    private_nh.param("Path_topic_Dijkstra", Path_topic_Dijkstra, std::string("/Plan_Dijkstra"));
    private_nh.param("Init_Pose_topic", Init_Pose_topic, std::string("init_pose"));
    private_nh.param("Goal_Pose_topic", Goal_Pose_topic, std::string("goal_pose"));
    private_nh.param("freq", freq, 0.5);

    //we'll subscribe to the latched topic that the map server uses
    ROS_INFO("Planner: Requesting the map");
    mapSubscriber = private_nh.subscribe(map_topic, 1, &PlannerForPred::mapCallback, this);
    initPoseSubscriber =  private_nh.subscribe(Init_Pose_topic, 1, &PlannerForPred::initPoseCallback, this);
    goalPoseSubscriber =  private_nh.subscribe(Goal_Pose_topic, 1, &PlannerForPred::goalPoseCallback, this);

    ros::Rate r(1.0);

    //Since a Base Map is needed, This cycle locks the program until the map has been correctly initialized
    while(!map_initialized_ && ros::ok())
    {
    	ros::spinOnce();
    	ROS_INFO("Planner: Still waiting on map...");
    	r.sleep();
    }
    // Once the map is received the program continues.
    ROS_INFO("Planner: Received a %d X %d map at %f m/pix\n",
    		map_meta_data_.width, map_meta_data_.height, map_meta_data_.resolution);

    // advertise the planned path
    pathPublisherAstar = private_nh.advertise<nav_msgs::Path>(Path_topic_Astar, 1);
    pathPublisherDijkstra = private_nh.advertise<nav_msgs::Path>(Path_topic_Dijkstra, 1);
    plannedPath.header.frame_id = GlobalMap_.header.frame_id;
    pose_at_x.header.frame_id = GlobalMap_.header.frame_id;
    srand(time(NULL));

    timer_ = private_nh.createTimer(ros::Duration(1.0/freq), &PlannerForPred::spin, this);


    ROS_INFO("Planner: Initialization Finished");
   // getPlanAStar();
}

PlannerForPred::~PlannerForPred() {
	// TODO Auto-generated destructor stub
}
void PlannerForPred::spin(const ros::TimerEvent& e)
{
	timer_.stop();
	//getTwoPlans();
	//getPlanDijkstra();
	//timer_.start();
}
void PlannerForPred::initPoseCallback(const geometry_msgs::PoseWithCovarianceStampedPtr& initPoseMsg)
{

  //ROS_INFO("Planner: Setting init pose (%f - %f)", initPoseMsg->pose.pose.position.x, initPoseMsg->pose.pose.position.y);
	currentPose.header = initPoseMsg->header;
	currentPose.pose = initPoseMsg->pose.pose;
}
void PlannerForPred::goalPoseCallback(const geometry_msgs::PoseStampedConstPtr& goalPoseMsg)
{
  //ROS_INFO("Planner: Setting goal pose (%f - %f)", goalPoseMsg->pose.position.x, goalPoseMsg->pose.position.y);
	geometry_msgs::PoseStamped goalPose;
	goalPose.header = goalPoseMsg->header;
	goalPose.pose = goalPoseMsg->pose;
  //getPlanDijkstra(currentPose, goalPose);
	getPlanAStar(currentPose, goalPose);
}

void PlannerForPred::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& mapMsg)
{

	 if(!map_initialized_) // Initialization flag set to true.
	 {
	     map_initialized_ = true;
	     ROS_INFO("Planner: Map Initialized Ok..");
	 }
	 // Copy the Map to a global variable.
	 GlobalMap_.header = mapMsg->header;
	 GlobalMap_.info = mapMsg->info;
	 GlobalMap_.data = mapMsg->data;
	 map_meta_data_ = mapMsg->info;

}
bool PlannerForPred::checkPositonInMap(unsigned int &posX, unsigned int &posY)
{
	return checkPositonInMap(posX, posY, GlobalMap_.info.width,  GlobalMap_.info.height);
}
bool PlannerForPred::checkPositonInMap(unsigned int &posX, unsigned int &posY, unsigned int MaxSizeX, unsigned int MaxSizeY)
{
	bool inMap = true;
	// Check if the point is with in the global map limits.
	if(posX > MaxSizeX)
	{
		posX = MaxSizeX;	// if not change it to the limit.
		inMap = false;

	}
	// Check if the point Y is within the global map limits.
	if(posY > MaxSizeY)
	{
		posY = MaxSizeY; // if not change it to the limit.
		inMap = false;
	}
	return inMap;
}

void PlannerForPred::mapToWorld(unsigned int mx, unsigned int my, double& wx, double& wy)
const {
	wx = GlobalMap_.info.origin.position.x + (mx + 0.5) * GlobalMap_.info.resolution;
	wy = GlobalMap_.info.origin.position.y + (my + 0.5) * GlobalMap_.info.resolution;
}

void PlannerForPred::worldToMap(const geometry_msgs::PoseStamped& Pose, unsigned int& mx, unsigned int& my)
const {

	worldToMap(Pose.pose.position.x, Pose.pose.position.y, mx, my);

}
void PlannerForPred::worldToMap(double wx, double wy, unsigned int& mx, unsigned int& my)
const {
	mx = (unsigned int) ((wx - GlobalMap_.info.origin.position.x) / GlobalMap_.info.resolution);
	my = (unsigned int) ((wy - GlobalMap_.info.origin.position.y) / GlobalMap_.info.resolution);
}
void PlannerForPred::getTwoPlans()
{
	int index;
	unsigned int posX_init;
	unsigned int posY_init;
	unsigned int posX_goal;
	unsigned int posY_goal;

//	unsigned int MAP_HEIGHT = 500;
//	unsigned int posX_init = rand()%MAP_HEIGHT + 200;
//	unsigned int posY_init = rand()%MAP_HEIGHT + 200;
//	unsigned int posX_goal = rand()%MAP_HEIGHT + 200;
//	unsigned int posY_goal = rand()%MAP_HEIGHT + 200;
	do {
		posX_init = rand()%GlobalMap_.info.width; //500 + 300;//
		posY_init = rand()%GlobalMap_.info.height; //500 + 300;//
		index = GlobalMap_.info.width * posY_init + posX_init;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Init: %d", GlobalMap_.data[index]);

	do {
		posX_goal = rand()%GlobalMap_.info.width; //500 + 300;//
		posY_goal = rand()%GlobalMap_.info.height; //500 + 300;//
		index = GlobalMap_.info.width * posY_goal + posX_goal;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Goal: %d",GlobalMap_.data[index]);
	//checkPositonInMap(posX_init, posY_init);
	//checkPositonInMap(posX_goal, posY_goal);
	ros::Time Tstart = ros::Time::now();
	getPlanAStar(posX_init, posY_init, posX_goal, posY_goal);
	ROS_INFO("Planner: A Star time: %f", ros::Time::now().toSec() - Tstart.toSec());
	Tstart = ros::Time::now();
	getPlanDijkstra(posX_init, posY_init, posX_goal, posY_goal);
	ROS_INFO("Planner: Dijkstra time: %f", ros::Time::now().toSec() - Tstart.toSec());

}
void PlannerForPred::getPlanAStar()
{
	int index;
	unsigned int posX_init;
	unsigned int posY_init;
	unsigned int posX_goal;
	unsigned int posY_goal;

//	unsigned int MAP_HEIGHT = 500;
//	unsigned int posX_init = rand()%MAP_HEIGHT + 200;
//	unsigned int posY_init = rand()%MAP_HEIGHT + 200;
//	unsigned int posX_goal = rand()%MAP_HEIGHT + 200;
//	unsigned int posY_goal = rand()%MAP_HEIGHT + 200;
	do {
		posX_init = rand()%GlobalMap_.info.width;
		posY_init = rand()%GlobalMap_.info.height;
		index = GlobalMap_.info.width * posY_init + posX_init;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Init: %d", GlobalMap_.data[index]);

	do {
		posX_goal = rand()%GlobalMap_.info.width;
		posY_goal = rand()%GlobalMap_.info.height;
		index = GlobalMap_.info.width * posY_goal + posX_goal;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Goal: %d",GlobalMap_.data[index]);
	//checkPositonInMap(posX_init, posY_init);
	//checkPositonInMap(posX_goal, posY_goal);
	ros::Time Tstart = ros::Time::now();
	getPlanAStar(posX_init, posY_init, posX_goal, posY_goal);
	ROS_INFO("Exec time: %f", ros::Time::now().toSec() - Tstart.toSec());

}

void PlannerForPred::getPlanAStar(const geometry_msgs::PoseStamped& initPose, const geometry_msgs::PoseStamped& goalPose)
{
	unsigned int posX_init;
	unsigned int posY_init;
	unsigned int posX_goal;
	unsigned int posY_goal;

	worldToMap(initPose, posX_init, posY_init);
	worldToMap(goalPose, posX_goal, posY_goal);

	checkPositonInMap(posX_init, posY_init);
	checkPositonInMap(posX_goal, posY_goal);

	getPlanAStar(posX_init, posY_init, posX_goal, posY_goal);
}

void PlannerForPred::getPlanAStar(const unsigned int posX_init, const unsigned int posY_init, const unsigned int posX_goal, const unsigned int posY_goal)
{
	//check whether initPose and Goal are whitin the map.

	PlannerAStar myAStarSearch(GlobalMap_);// = new PlannerAStar();


	// Create a start state
	Node nodeStart;
	nodeStart.setPosefromXY(posX_init, posY_init);

	// Define the goal state
	Node nodeEnd;
	nodeEnd.setPosefromXY(posX_goal, posY_goal);

	// Set Start and goal states
	ROS_INFO("Planner: Start: (%f ,%f) - Goal (%f ,%f) ", nodeStart.getPosX(),nodeStart.getPosY(), nodeEnd.getPosX(),nodeEnd.getPosY());

	myAStarSearch.SetStartAndGoalStates( nodeStart, nodeEnd );

	unsigned int SearchState;
	unsigned int SearchSteps = 0;
	do
	{
		SearchState = myAStarSearch.SearchStep();
		SearchSteps++;

//
//		int len = 0;
//
//		ROS_INFO("Planner: Open Nodes");
//		//		Node *p = myAStarSearch->GetOpenListStart();
//		Node *p = myAStarSearch.GetOpenListStart();
//
//
//		while( p )
//		{
//			len++;
//			((Node *)p)->PrintNodeInfo();
////			p = myAStarSearch->GetOpenListNext();
//			p = myAStarSearch.GetOpenListNext();
//
//		}
//		ROS_INFO("Planner: Open list has %d Nodes",len);

//		len = 0;
//		ROS_INFO("Planner: Closed");
//
//	//	p = myAStarSearch->GetClosedListStart();
//		p = myAStarSearch.GetClosedListStart();
//
//		while( p )
//		{
//			len++;
//			p->PrintNodeInfo();
//			//p = myAStarSearch->GetClosedListNext();
//			p = myAStarSearch.GetClosedListNext();
//		}
//		ROS_INFO("Planner: Closed list has %d Nodes",len);

	}while( SearchState == myAStarSearch.SEARCH_STATE_SEARCHING);

	if( SearchState == myAStarSearch.SEARCH_STATE_SUCCEEDED )
	{

//		ROS_INFO("Planner: Search found goal state");
		Node *node = myAStarSearch.GetSolutionStart();

		int steps = 0;
		double posxWorld;
		double posyWorld;
	//	node->PrintNodeInfo();
		// set data for path publisher
		plannedPath.poses.clear();
		plannedPath.header.stamp = ros::Time::now();
		plannedPath.header.seq = 1;
		pose_at_x.header.stamp = plannedPath.header.stamp;


		for( ;; )
		{
			//node = myAStarSearch->GetSolutionNext();
			node = myAStarSearch.GetSolutionNext();
			if( !node )
			{
				break;
			}
			//node->PrintNodeInfo();
			steps ++;

			mapToWorld(node->getPosX(), node->getPosY(), posxWorld, posyWorld);
			pose_at_x.pose.position.x = posxWorld;
			pose_at_x.pose.position.y = posyWorld;
			pose_at_x.header.seq = steps;
			//ROS_INFO("Planner: Solution steps %d - pose x %f - %f, pose y: %f - %f", steps, node->getPosX(), pose_at_x.pose.position.x, node->getPosY(), pose_at_x.pose.position.y);
			plannedPath.poses.push_back(pose_at_x);
		};
    //ROS_INFO("Planner: Solution steps %d path size %d", steps, (int)(plannedPath.poses.size()));
		// Once you're done with the solution you can free the nodes up
		//myAStarSearch->FreeSolutionNodes();
		myAStarSearch.FreeSolutionNodes();
		pathPublisherAstar.publish(plannedPath);
	}
	//else if( SearchState == myAStarSearch->SEARCH_STATE_FAILED )
	else if( SearchState == myAStarSearch.SEARCH_STATE_FAILED )
	{
		ROS_INFO("Planner: Search terminated. Did not find goal state");
	}
	else if ( SearchState == myAStarSearch.SEARCH_STATE_OUT_OF_MEMORY)
	{
		ROS_INFO("Planner: Out of memory");

	}
	// Display the number of loops the search went through
  //ROS_INFO("Planner: SearchSteps %d ", SearchSteps);
	//myAStarSearch->EnsureMemoryFreed();
	//myAStarSearch.EnsureMemoryFreed();
	//delete myAStarSearch;
}

void PlannerForPred::getPlanDijkstra()
{
	int index;
	unsigned int posX_init;
	unsigned int posY_init;
	unsigned int posX_goal;
	unsigned int posY_goal;

//	unsigned int MAP_HEIGHT = 25;
//	posX_init = rand()%MAP_HEIGHT;
//	posY_init = rand()%MAP_HEIGHT;
//	posX_goal = rand()%MAP_HEIGHT;
//	posY_goal = rand()%MAP_HEIGHT;
	do {
		posX_init = rand()%GlobalMap_.info.width;
		posY_init = rand()%GlobalMap_.info.height;
		index = GlobalMap_.info.width * posY_init + posX_init;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Init: %d", GlobalMap_.data[index]);

	do {
		posX_goal = rand()%GlobalMap_.info.width;
		posY_goal = rand()%GlobalMap_.info.height;
		index = GlobalMap_.info.width * posY_goal + posX_goal;
	} while (GlobalMap_.data[index] != 0);
	ROS_INFO("Map Value Goal: %d",GlobalMap_.data[index]);
	//checkPositonInMap(posX_init, posY_init);
	//checkPositonInMap(posX_goal, posY_goal);
	ros::Time Tstart = ros::Time::now();
	getPlanDijkstra(posX_init, posY_init, posX_goal, posY_goal);
	ROS_INFO("Exec time: %f", ros::Time::now().toSec() - Tstart.toSec());

}

void PlannerForPred::getPlanDijkstra(const geometry_msgs::PoseStamped& initPose, const geometry_msgs::PoseStamped& goalPose)
{
	unsigned int posX_init;
	unsigned int posY_init;
	unsigned int posX_goal;
	unsigned int posY_goal;

	worldToMap(initPose, posX_init, posY_init);
	worldToMap(goalPose, posX_goal, posY_goal);

	if( checkPositonInMap(posX_init, posY_init) &&	checkPositonInMap(posX_goal, posY_goal))
	{
		getPlanDijkstra(posX_init, posY_init, posX_goal, posY_goal);
	}
	else
	{
		ROS_INFO("Point not in Map");
	}

}

void PlannerForPred::getPlanDijkstra(const unsigned int posX_init, const unsigned int posY_init, const unsigned int posX_goal, const unsigned int posY_goal)
{
	//check whether initPose and Goal are whitin the map.

	PlannerDijkstra myDijkstraSearch(GlobalMap_);// = new PlannerAStar();


	// Create a start state
	Node nodeStart;
	nodeStart.setPosefromXY(posX_init, posY_init);

	// Define the goal state
	Node nodeEnd;
	nodeEnd.setPosefromXY(posX_goal, posY_goal);

	// Set Start and goal states
	ROS_INFO("Planner: Start: (%f ,%f) - Goal (%f ,%f) ", nodeStart.getPosX(),nodeStart.getPosY(), nodeEnd.getPosX(),nodeEnd.getPosY());

	myDijkstraSearch.SetStartAndGoalStates( nodeStart, nodeEnd );

	unsigned int SearchState;
	unsigned int SearchSteps = 0;
	do
	{
		SearchState = myDijkstraSearch.SearchStep();
		SearchSteps++;
//
//
//		int lenOpen = 0;
//
//		//ROS_INFO("Planner: Open Nodes");
//		Node *p = myDijkstraSearch.GetOpenListStart();
//
//
//		while( p )
//		{
//			lenOpen++;
//			//((Node *)p)->PrintNodeInfo();
//			p = myDijkstraSearch.GetOpenListNext();
//
//		}
//		//ROS_INFO("Planner: Open list has %d Nodes",len);
//
//		int lenClosed = 0;
//		//ROS_INFO("Planner: Closed Nodes");
//
//		p = myDijkstraSearch.GetClosedListStart();
//
//		while( p )
//		{
//			lenClosed++;
//			//p->PrintNodeInfo();
//			p = myDijkstraSearch.GetClosedListNext();
//		}
//		//ROS_INFO("Planner: Closed list has %d Nodes",len);
//		int lenNeig = 0;
//
//		//ROS_INFO("Planner: Neighbor Nodes");
//		p = myDijkstraSearch.GetNeighborListStart();
//
//		while( p )
//		{
//			lenNeig++;
//			//p->PrintNodeInfo();
//			p = myDijkstraSearch.GetNeighborListNext();
//		}
//		ROS_INFO("Planner: Open: %d, Closed: %d, Neighbor %d Nodes",lenOpen, lenClosed, lenNeig);

	}while( SearchState == myDijkstraSearch.SEARCH_STATE_SEARCHING);

	if( SearchState == myDijkstraSearch.SEARCH_STATE_SUCCEEDED )
	{

	//	ROS_INFO("Planner: Search found goal state");

		Node *node = myDijkstraSearch.GetSolutionStart();
	//	ROS_INFO("Planner: Displaying solution");

		int steps = 0;
		double posxWorld;
		double posyWorld;
		//node->PrintNodeInfo();
		// set data for path publisher
		plannedPath.poses.clear();
		plannedPath.header.stamp = ros::Time::now();
		plannedPath.header.seq = 1;
		pose_at_x.header.stamp = plannedPath.header.stamp;


		for( ;; )
		{

			node = myDijkstraSearch.GetSolutionNext();
			if( !node )
			{
				break;
			}
		//	node->PrintNodeInfo();
			steps ++;

			mapToWorld(node->getPosX(), node->getPosY(), posxWorld, posyWorld);
			pose_at_x.pose.position.x = posxWorld;
			pose_at_x.pose.position.y = posyWorld;
			pose_at_x.header.seq = steps;
			//ROS_INFO("Planner: Solution steps %d - pose x %f - %f, pose y: %f - %f", steps, node->getPosX(), pose_at_x.pose.position.x, node->getPosY(), pose_at_x.pose.position.y);
			plannedPath.poses.push_back(pose_at_x);
		};
		ROS_INFO("Planner: Solution steps %d path size %d", steps, (int)(plannedPath.poses.size()));
		// Once you're done with the solution you can free the nodes up

		myDijkstraSearch.FreeSolutionNodes();
		pathPublisherDijkstra.publish(plannedPath);
	}

	else if( SearchState == myDijkstraSearch.SEARCH_STATE_FAILED )
	{
		ROS_INFO("Planner: Search terminated. Did not find goal state");
	}
	else if ( SearchState == myDijkstraSearch.SEARCH_STATE_OUT_OF_MEMORY)
	{
		ROS_INFO("Planner: Out of memory");

	}
	// Display the number of loops the search went through
	ROS_INFO("Planner: SearchSteps %d ", SearchSteps);

}
