/*
 * PlannerDijkstra.cpp
 *
 *  Created on: Mar 20, 2013
 *      Author: garzonov
 */

#include <planner_upm/planner_dijkstra.h>



PlannerDijkstra::PlannerDijkstra(nav_msgs::OccupancyGrid map_):
		PlannerTools(map_)
{
	// TODO Auto-generated constructor stub

}

PlannerDijkstra::~PlannerDijkstra() {
	// TODO Auto-generated destructor stub
}


unsigned int PlannerDijkstra::SearchStep()
{
	// Firstly break if the user has not initialised the search
	assert( (m_State > SEARCH_STATE_NOT_INITIALISED) &&
			(m_State < SEARCH_STATE_INVALID) );
//	ROS_INFO("Search Step: pass assert");
	// Next I want it to be safe to do a searchstep once the search has succeeded...
	if( (m_State == SEARCH_STATE_SUCCEEDED) ||
		(m_State == SEARCH_STATE_FAILED)
	  )
	{
		return m_State;
	}

	// Failure is defined as emptying the open list as there is nothing left to
	// search...
	// New: Allow user abort
	if( m_OpenList.empty() || m_CancelRequest )
	{
		ROS_WARN("PlannerDijkstra - Search Step: Open list empty");
		FreeAllNodes();
		m_State = SEARCH_STATE_FAILED;
		return m_State;
	}

	// Incremement step count
	m_Steps ++;


	// Pop the best node (the one with the lowest f)
	make_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );
	Node *n = m_OpenList.front();

	// This is done just to delete the best of the vector...
	m_OpenList.erase(m_OpenList.begin());

	//ROS_INFO("PlannerDijkstra - Search Step: pass pop_heap");
	//n->PrintNodeInfo();

	// Check for the goal, once we pop that we're done
	if( n->IsGoal(*m_Goal) )
	{
		ROS_INFO("PlannerDijkstra - Goal Found");
		// The user is going to use the Goal Node he passed in
		// so copy the parent pointer of n
		m_Goal->parentNode = n->parentNode;

		// A special case is that the goal was passed in as the start state
		// so handle that here
		if( false == n->IsSameState(*m_Start) )
		{
			delete n;

			// set the child pointers in each node (except Goal which has no child)
			Node *nodeChild = m_Goal;
			Node *nodeParent = m_Goal->parentNode;
			//ROS_INFO("PlannerDijkstra - Search Step: getting back the route");

			do
			{
				nodeParent->childNode = nodeChild;

				//nodeChild->PrintNodeInfo();
				nodeChild = nodeParent;
				nodeParent = nodeParent->parentNode;
//				if(nodeParent == NULL && nodeChild != m_Start)
//				{
//					ROS_ERROR("Where is my Daddy :(");
//					return SEARCH_STATE_FAILED;
//				}
			}
			while( nodeChild != m_Start ); // Start is always the first node by definition

		}
		//ROS_INFO("PlannerDijkstra - Search Step: Free Unused Nodes");
		// delete nodes that aren't needed for the solution
		FreeUnusedNodes();

		m_State = SEARCH_STATE_SUCCEEDED;

		return m_State;
	}
	else
	{
		// We now need to generate the successors of this node
		// The user helps us to do this, and we keep the new nodes in
		// m_Successors ...

		m_Successors.clear(); // empty vector of successor nodes to n

		// User provides this functions and uses AddSuccessor to add each successor of
		// node 'n' to m_Successors
		bool ret = GetSuccessors( n, n->parentNode ? n->parentNode : NULL );
		//ROS_INFO("PlannerDijkstra - Search Step: Got %d successors", (int)(m_Successors.size()) );
		if( !ret )
		{

			typename std::vector< Node * >::iterator successor;

			// free the nodes that may previously have been added
			for( successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
			{
				delete (*successor);
			}

			m_Successors.clear(); // empty vector of successor nodes to n

			// free up everything else we allocated
			FreeAllNodes();

			ROS_WARN("PlannerDijkstra - Search Step: Fail to get successors");

			m_State = SEARCH_STATE_OUT_OF_MEMORY;
			return m_State;
		}

		// Now handle each successor to the current node ...

		for( typename std::vector< Node * >::iterator successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
		{
			// 	The g value for this successor from the cost map (may be zero)
			//double newg = n->g_score + n->GoalDistanceEstimate(*(*successor));
	//		(*successor)->PrintNodeInfo();
			//getchar();
			double newg_score = n->g_score + (*successor)->g_score;
			double newf_score= n->f_score + (*successor)->f_score;

			// Now we need to find whether the node is on the open or closed lists
			// If it is but the node that is already on them is better (lower g)
			// then we can forget about this successor

			// First linear search of open list to find node
			typename std::vector< Node * >::iterator openlist_result;
			for( openlist_result = m_OpenList.begin(); openlist_result != m_OpenList.end(); openlist_result ++ )
			{
				if( (*openlist_result)->IsSameState( *(*successor)) )
				{
					break;
				}
			}

			if( openlist_result != m_OpenList.end() )
			{

				// we found this state on open

				if( (*openlist_result)->f_score <= newf_score )
				{

					delete (*successor);

					// the one on Open is cheaper than this one
					continue;
				}
			}

			typename std::vector< Node * >::iterator neighborslist_result;

			for( neighborslist_result = m_neighbors.begin(); neighborslist_result != m_neighbors.end(); neighborslist_result ++ )
			{
				if( (*neighborslist_result)->IsSameState( *(*successor) ))
				{
					break;
				}
				if( (*neighborslist_result)->g_score < newg_score - 2.5*SQR2 )
				{
					//ROS_INFO("Deleting m_neighbors....neigh score : %f - new %f",(*neighborslist_result)->g_score, newg_score);
					//m_neighbors.erase(neighborslist_result);
					//printf("..ok\n");
				}
			}

			if( neighborslist_result != m_neighbors.end() )
			{

				// we found this state on closed

				if( (*neighborslist_result)->f_score <= newf_score )
				{
					// the one on Closed is cheaper than this one

					delete (*successor);

					continue;
				}
			}

			// This node is the best node so far with this particular state
			// so lets keep it and set up its AStar specific data ...
			// Add the distance to the successor.

			(*successor)->parentNode = n;
			(*successor)->g_score = newg_score;
			(*successor)->f_score = newf_score;//(*successor)->g_score + (*successor)->h_score;

			// Remove successor from closed if it was on it
			if( neighborslist_result != m_neighbors.end() )
			{
				ROS_INFO("Fund in closed old : %f - new %f",(*neighborslist_result)->f_score, newf_score);
				//ROS_INFO("Closed list if");
				// remove it from Closed

				m_neighbors.erase( neighborslist_result );

				typename std::vector< Node * >::iterator iterClosed;
				for( iterClosed = m_ClosedList.begin(); iterClosed != m_ClosedList.end(); iterClosed ++ )
				{
					if( (*iterClosed)->IsSameState( *(*neighborslist_result) ))
						break;
				}
				m_ClosedList.erase( iterClosed );
				delete (*neighborslist_result);
				//m_ClosedList.erase( neighborslist_result );



			}

			// Update old version of this node
			if( openlist_result != m_OpenList.end() )
			{
				//ROS_INFO("Open list if");
				delete (*openlist_result);
				m_OpenList.erase( openlist_result );
			}

			// heap now unsorted
			m_OpenList.push_back( (*successor) );

		}
		// push n onto Closed, as we have expanded it now
		m_ClosedList.push_back( n );
		m_neighbors.push_back( n );
	}// end else (not goal so expand)

return m_State; // Succeeded bool is false at this point.

}
Node *PlannerDijkstra::GetNeighborListStart()
{
	iterDbgClosed = m_neighbors.begin();
	if( iterDbgClosed != m_neighbors.end() )
	{
		return (*iterDbgClosed);
	}

	return NULL;
}

Node *PlannerDijkstra::GetNeighborListNext()
{
	iterDbgClosed++;
	if( iterDbgClosed != m_neighbors.end() )
	{
		return (*iterDbgClosed);
	}

	return NULL;
}
bool PlannerDijkstra::GetSuccessors(Node *node, Node *parent_node )
{

	int parent_x;
	int parent_y;
	int dir;
	int newPosX;
	int newPosY;

	if( parent_node == NULL)
	{
		parent_x = -1;
		parent_y = -1;
	}
	else
	{
		parent_x = parent_node->getPosX();
		parent_y = parent_node->getPosY();
	}


	Node NewNode;
	// push each possible move except allowing the search to go backwards
	for(dir = 0; dir < MaxDir; dir++)
	{
		newPosX = node->getPosX() + deltaX[dir];
		newPosY = node->getPosY() + deltaY[dir];
		if (isNodeInMap(newPosX, newPosY))
		{
			if( (getMapValue( newPosX, newPosY ) < 90) //Only Free nodes
					&& !((parent_x == newPosX) && (parent_y == newPosY)))
			{
				NewNode = Node( newPosX, newPosY );
				NewNode.g_score = ((deltaX[dir] == 0) || (deltaY[dir] == 0)) ? 1 : SQR2;
				NewNode.h_score = getMapValue(newPosX, newPosY);
				NewNode.f_score = NewNode.g_score + NewNode.h_score;
				AddSuccessor( NewNode );
			}
		}
	}
	return true;
}
void PlannerDijkstra::SetStartAndGoalStates( Node &Start, Node &Goal )
{
	m_CancelRequest = false;

	m_Start = new Node(Start);
	m_Goal =  new Node(Goal);


	assert((m_Start != NULL && m_Goal != NULL));

	m_State = SEARCH_STATE_SEARCHING;

	m_Start->set_g_score(0);
	m_Start->set_h_score(getMapValue(m_Start->getPosX(),m_Start->getPosX()));
	m_Start->set_f_score(m_Start->g_score + m_Start->h_score);
	m_Start->parentNode = 0;

	// Push the start node on the Open list
	m_OpenList.push_back(m_Start); // heap now unsorted

	// Sort back element into heap
	push_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );

	// Initialise counter for search steps
	m_Steps = 0;
//	ROS_INFO("PlannerDijkstra: Set Start And Goal States ok");

}
