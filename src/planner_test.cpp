/*
 * planner_test.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: garzonov
 */

#include <planner_upm/planner_prediction.h>
class PlannerForPredNode
{
    public:
	PlannerForPredNode(tf::TransformListener& tf) : plannerForPred_(tf){}
    private:
	PlannerForPred plannerForPred_;

  };


int main(int argc, char **argv)
{
  // Initialize ROS
  ros::init(argc, argv, "Planner_Prediction");

  tf::TransformListener tf(ros::Duration(10));

  // create the Test node class
  PlannerForPredNode* myPlanner;
  myPlanner = new PlannerForPredNode(tf);
  // Start Spinning
  ros::spin();

  delete myPlanner;

  return 0;
}
