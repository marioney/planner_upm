/*
 * PlannerAStar.cpp
 *
 *  Created on: Mar 13, 2013
 *      Author: garzonov
 */

#include <planner_upm/planner_a_star.h>

PlannerAStar::PlannerAStar(nav_msgs::OccupancyGrid map_):
PlannerTools(map_)
{
	// TODO Auto-generated constructor stub

}

PlannerAStar::~PlannerAStar() {
	// TODO Auto-generated destructor stub
}

void PlannerAStar::SetStartAndGoalStates( Node &Start, Node &Goal )
{
	m_CancelRequest = false;

	m_Start = new Node(Start);
	m_Goal =  new Node(Goal);

	assert((m_Start != NULL && m_Goal != NULL));

//	m_Start->setPosefromXY() = Start;
//	m_Goal->m_UserState = Goal;

	m_State = SEARCH_STATE_SEARCHING;

	// Initialise the AStar specific parts of the Start Node
	// The user only needs fill out the state information

	m_Start->set_g_score(0);
	m_Start->set_h_score(m_Start->GoalDistanceEstimate(*m_Goal));
	m_Start->set_f_score(m_Start->g_score + m_Start->h_score);
	m_Start->parentNode = 0;

	// Push the start node on the Open list
	m_OpenList.push_back( m_Start ); // heap now unsorted

	// Sort back element into heap
	push_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );

	// Initialise counter for search steps
	m_Steps = 0;
}
unsigned int PlannerAStar::SearchStep()
{
	// Firstly break if the user has not initialised the search
	assert( (m_State > SEARCH_STATE_NOT_INITIALISED) &&
			(m_State < SEARCH_STATE_INVALID) );
	//	ROS_INFO("Search Step: pass assert");

	// Next I want it to be safe to do a searchstep once the search has succeeded...
	if( (m_State == SEARCH_STATE_SUCCEEDED) ||
		(m_State == SEARCH_STATE_FAILED)
	  )
	{
		return m_State;
	}

	// Failure is defined as emptying the open list as there is nothing left to
	// search...
	// New: Allow user abort
	if( m_OpenList.empty() || m_CancelRequest )
	{
		ROS_INFO("Search Step: Open list empty");
		FreeAllNodes();
		m_State = SEARCH_STATE_FAILED;
		return m_State;
	}

	// Increment step count
	m_Steps ++;

	// Re-Sort the vector in terms of the lower f_score
	make_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );
	// Pop the best node (the one with the lowest f)
	Node *n = m_OpenList.front();
	// This is done just to delete the best one from the vector of open nodes
	m_OpenList.erase(m_OpenList.begin());
	//n->PrintNodeInfo();
	//ROS_INFO("Search Step: pass pop_heap");

	// Check for the goal, once we pop that we're done
	if( n->IsGoal(*m_Goal) )
	{
		// The user is going to use the Goal Node he passed in
		// so copy the parent pointer of n
		ROS_INFO("PlannerAStar - Goal Found");
		m_Goal->parentNode = n->parentNode;

		// A special case is that the goal was passed in as the start state
		// so handle that here
		if( false == n->IsSameState(*m_Start) )
		{
			delete n;

			// set the child pointers in each node (except Goal which has no child)
			Node *nodeChild = m_Goal;
			Node *nodeParent = m_Goal->parentNode;

			do
			{
				nodeParent->childNode = nodeChild;
				nodeChild = nodeParent;
				nodeParent = nodeParent->parentNode;
				if(nodeParent == NULL && nodeChild != m_Start)
				{
					ROS_ERROR("Where is my Daddy :(");
					return SEARCH_STATE_FAILED;
				}

			}
			while( nodeChild != m_Start ); // Start is always the first node by definition

		}

		// delete nodes that aren't needed for the solution
		FreeUnusedNodes();

		m_State = SEARCH_STATE_SUCCEEDED;

		return m_State;
	}
	else // not goal
	{

		// We now need to generate the successors of this node
		// The user helps us to do this, and we keep the new nodes in
		// m_Successors ...

		m_Successors.clear(); // empty vector of successor nodes to n
	//	ROS_INFO("Search Step: Got %d succesors", m_Successors.size() );

		// User provides this functions and uses AddSuccessor to add each successor of
		// node 'n' to m_Successors
		bool ret = GetSuccessors( n, n->parentNode ? n->parentNode : NULL );
		if( !ret )
		{

			typename std::vector< Node * >::iterator successor;

			// free the nodes that may previously have been added
			for( successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
			{
				delete (*successor);
			}

			m_Successors.clear(); // empty vector of successor nodes to n

			// free up everything else we allocated
			FreeAllNodes();

			m_State = SEARCH_STATE_OUT_OF_MEMORY;
			return m_State;
		}

		// Now handle each successor to the current node ...
		for( typename std::vector< Node * >::iterator successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
		{
			// 	The g value for this successor from the cost map (may be zero)
			//double newg = n->g_score + GetCost(*(*successor));
			//double newg = n->g_score + n->GoalDistanceEstimate( *(*successor));
			double newg = n->g_score + (*successor)->g_score;




			// Now we need to find whether the node is on the open or closed lists
			// If it is but the node that is already on them is better (lower g)
			// then we can forget about this successor

			// First linear search of open list to find node

			typename std::vector< Node * >::iterator openlist_result;

			for( openlist_result = m_OpenList.begin(); openlist_result != m_OpenList.end(); openlist_result ++ )
			{
				if( (*openlist_result)->IsSameState( *(*successor)) )
				{
					break;
				}
			}

			if( openlist_result != m_OpenList.end() )
			{

				// we found this state on open

				if( (*openlist_result)->g_score <= newg )
				{
					delete (*successor);

					// the one on Open is cheaper than this one
					continue;
				}
			}

			typename std::vector< Node * >::iterator closedlist_result;

			for( closedlist_result = m_ClosedList.begin(); closedlist_result != m_ClosedList.end(); closedlist_result ++ )
			{
				if( (*closedlist_result)->IsSameState( *(*successor) ))
				{
					break;
				}
			}

			if( closedlist_result != m_ClosedList.end() )
			{

				// we found this state on closed

				if( (*closedlist_result)->g_score <= newg )
				{
					// the one on Closed is cheaper than this one
					delete (*successor);

					continue;
				}
			}

			// This node is the best node so far with this particular state
			// so lets keep it and set up its AStar specific data ...
			// Add the distance to the successor.

			(*successor)->parentNode = n;
			(*successor)->g_score = newg;
			(*successor)->h_score = (*successor)->EuclideanDistance(*m_Goal);
			(*successor)->f_score = (*successor)->g_score + (*successor)->h_score;

			// Remove successor from closed if it was on it

			if( closedlist_result != m_ClosedList.end() )
			{
				//ROS_INFO("Closed list if");
				// remove it from Closed
				delete (*closedlist_result);
				m_ClosedList.erase( closedlist_result );

				// Fix thanks to ...
				// Greg Douglas <gregdouglasmail@gmail.com>
				// who noticed that this code path was incorrect
				// Here we have found a new state which is already CLOSED
				// anus


			}

			// Update old version of this node
			if( openlist_result != m_OpenList.end() )
			{
				//ROS_INFO("Open list if");
				delete (*openlist_result);
				m_OpenList.erase( openlist_result );

				// re-make the heap
				// make_heap rather than sort_heap is an essential bug fix
				// thanks to Mike Ryynanen for pointing this out and then explaining
				// it in detail. sort_heap called on an invalid heap does not work
				//make_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );

			}

			// heap now unsorted
			m_OpenList.push_back( (*successor) );
			// sort back element into heap
			//push_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );

		}

		// push n onto Closed, as we have expanded it now

		m_ClosedList.push_back( n );

	} // end else (not goal so expand)

	return m_State; // Succeeded bool is false at this point.

}

bool PlannerAStar::GetSuccessors(Node *node, Node *parent_node )
{

	int parent_x;
	int parent_y;
	int dir;
	int newPosX;
	int newPosY;

	if( parent_node == NULL)
	{
		parent_x = -1;
		parent_y = -1;
	}
	else
	{
		parent_x = parent_node->getPosX();
		parent_y = parent_node->getPosY();
	}


	Node NewNode;
	// push each possible move except allowing the search to go backwards
	for(dir = 0; dir < MaxDir; dir++)
	{
		newPosX = node->getPosX() + deltaX[dir];
		newPosY = node->getPosY() + deltaY[dir];
		if (isNodeInMap(newPosX, newPosY))
		{
			if( (getMapValue( newPosX, newPosY ) < 90) //Only unoccupied nodes
					&& !((parent_x == newPosX) && (parent_y == newPosY)))
			{
				NewNode = Node( newPosX, newPosY );
				NewNode.g_score = ((deltaX[dir] == 0) || (deltaY[dir] == 0)) ? 1 : SQR2;
				NewNode.g_score += getMapValue( newPosX, newPosY );
				AddSuccessor( NewNode );
			}
		}
	}

////	if (NewNode.getPosX() == 0 && NewNode.getPosY() == 0) // No new nodes
////		return false;
	return true;
}


