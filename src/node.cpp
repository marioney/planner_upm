/*
 * Node.cpp
 *
 *  Created on: Mar 12, 2013
 *      Author: garzonov
 */

#include <planner_upm/node.h>


Node::Node()
{
	parentNode = NULL;
	childNode = NULL;
	h_score = 0;
	f_score = 0;
	g_score = 0;
	cost_value = 0;
	visited = false;

	// Init orientation
	nodePose_.orientation.w = 1.0;
	nodePose_.orientation.x = 0.0;
	nodePose_.orientation.y = 0.0;
	nodePose_.orientation.z = 0.0;

	// Init position
	nodePose_.position.x = 0.0;
	nodePose_.position.y = 0.0;
	nodePose_.position.z = 0.0;

}
Node::Node(Node &nodeInit)
{
	parentNode = nodeInit.parentNode;
	childNode = nodeInit.childNode;
	h_score = nodeInit.h_score;
	f_score = nodeInit.f_score;
	g_score = nodeInit.g_score;
	nodePose_ = nodeInit.nodePose_;
	cost_value = nodeInit.cost_value;
	visited = false;
}

Node::Node(geometry_msgs::Pose nodePose)
{
	parentNode = NULL;
	childNode = NULL;
	h_score = 0;
	f_score = 0;
	g_score = 0;
	cost_value = 0;
	nodePose_ = nodePose;
	visited = false;
}

Node::Node(double px, double py)
{
	parentNode = NULL;
	childNode = NULL;
	h_score = 0;
	f_score = 0;
	g_score = 0;
	cost_value = 0;
	// Init orientation
	nodePose_.orientation.w = 1.0;
	nodePose_.orientation.x = 0.0;
	nodePose_.orientation.y = 0.0;
	nodePose_.orientation.z = 0.0;
	nodePose_.position.z = 0.0;
	setPosefromXY(px, py);
}

void Node::setPosefromXY(double px, double py)
{
	nodePose_.position.x = px;
	nodePose_.position.y = py;
}
//TODO Add a good heuristic here
double Node::EuclideanDistance(Node &nodeGoal)
{
	double Distance;

	// For Euclidean Distance
	Distance = sqrt(EuclideanDistanceSquare(nodeGoal));

	return Distance;
}
double Node::EuclideanDistanceSquare(Node &nodeGoal)
{
	double Distance;

	double deltaX = getPosX() - nodeGoal.getPosX();
	double deltaY = getPosY() - nodeGoal.getPosY();

	//For Square Euclidean Distance
	Distance = deltaX*deltaX + deltaY*deltaY;

	return Distance;
}
double Node::ChebyshevDistance(Node &nodeGoal)
{
	double Distance;

	double deltaX = getPosX() - nodeGoal.getPosX();
	double deltaY = getPosY() - nodeGoal.getPosY();

	// Chebyshev distance
	Distance = std::max(fabs(deltaX), fabs(deltaY));

	return Distance;
}
double Node::ManhattanDistance(Node &nodeGoal)
{
	double Distance;

	double deltaX = getPosX() - nodeGoal.getPosX();
	double deltaY = getPosY() - nodeGoal.getPosY();

	// For Manhattan Distance
	Distance = fabs(deltaX) + fabs(deltaY);
	return Distance;
}
double Node::GoalDistanceEstimate(Node &nodeGoal)
{

	return EuclideanDistance(nodeGoal);
}

Node::~Node() {
	// TODO Auto-generated destructor stub
}
bool Node::IsSameState( Node &rhs )
{
	// same state in a maze search is simply when (x,y) are the same
	if( (getPosX() == rhs.getPosX()) &&
		(getPosY() == rhs.getPosY()) )
	{
		return true;
	}
	else
	{
		return false;
	}
}
void Node::PrintNodeInfo()
{
	ROS_INFO("Node position : (%f, %f) Scores: (%f) + (%f) = (%f) ", getPosX(),getPosY(), get_g_score(), get_h_score(), get_f_score());
}

bool Node::IsGoal( Node &nodeGoal )
{
	return IsSameState( nodeGoal );

//	if( (getPosX() == nodeGoal.getPosX()) &&
//		(getPosY() == nodeGoal.getPosY()) )
//	{
//		return true;
//	}
//
//	return false;
}

/*
float Node::GetCost( Node &successor )
{
	return (float) GetMap( x, y );

}
*/


