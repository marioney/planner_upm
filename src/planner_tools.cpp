/*
 * planner_tools.cpp
 *
 *  Created on: Apr 14, 2014
 *      Author: mario
 */

#include <planner_upm/planner_tools.h>
int PlannerTools::deltaX[] = {1, 1, 0, -1, -1, -1, 0, 1};
int PlannerTools::deltaY[] = {0, 1, 1, 1, 0, -1, -1, -1};
//int PlannerDijkstra::deltaX[] = {1, 0, -1, 0};
//int PlannerDijkstra::deltaY[] = {0, 1, 0, -1};
//int PlannerTools::MaxDir = sizeof(deltaX)/sizeof(int);

PlannerTools::PlannerTools(nav_msgs::OccupancyGrid map_):
m_State( SEARCH_STATE_NOT_INITIALISED ),
m_Steps(0)
{
	// TODO Auto-generated constructor stub

	//deltaX[8] = {1, 1, 0, -1, -1, -1, 0, 1};
	//deltaY[8] = {0, 1, 1, 1, 0, -1, -1, -1};
	MaxDir = sizeof(deltaX)/sizeof(int);
	m_AllocateNodeCount = 0;
	m_CancelRequest = false;
	m_CurrentSolutionNode = NULL;
	m_Start = NULL;
	m_Goal= NULL;
	MapThreshold = 199;
	costMap_ = map_;
}

PlannerTools::~PlannerTools() {
	// TODO Auto-generated destructor stub
}

int PlannerTools::getMapValue(const Node &node)
{
	return getMapValue(node.getPosX(), node.getPosY());

}
int PlannerTools::getMapValue(const unsigned int width, const unsigned int height)
{
	unsigned int position = costMap_.info.width * height + width;	// Finds the position in the data vector.
	if(position <= costMap_.data.size())
	{
		return costMap_.data[position];// Gets the value from the position.
	}
	ROS_ERROR("getMapValue: Index out of bounds");
	return -1;

}
void PlannerTools::FreeNodesInVector(std::vector< Node * > node_list, const bool keep_used)
{
	// Creates an iterator over the nodes list
	typename std::vector< Node * >::iterator iterList;


	//ROS_INFO("Nodes list size: %d",(int) (node_list.size()));

	if(node_list.size() == 0)
	{
		ROS_INFO("Trying to delete an empty list");
	}
	// Starts from the first element on the list
	// And goes one by one
	for( iterList = node_list.begin(); iterList != node_list.end(); iterList ++ )
	{
		Node *n = (*iterList);
		// Only nodes used for the solution have children
		// If it has not one, then it can be deleted
		if (n == NULL)
		{
			ROS_ERROR("Null value in list");
		}
		else
		{
			if(keep_used)
			{
				if( n->childNode )
				{
					continue;
				}
			}
			else
			{
				delete n;
				n = NULL;
			}
		}
	}
	//ROS_INFO("Nodes Deleted..");
}
void PlannerTools::FreeNodesInVector(std::vector< Node * > node_list)
{
	FreeNodesInVector(node_list,false);
}

void PlannerTools::FreeUnusedNodesInVector(std::vector< Node * > node_list)
{
	FreeNodesInVector(node_list,true);
}

void PlannerTools::FreeUnusedNodes()
{
	// Frees nodes in the open nodes list
	FreeUnusedNodesInVector(m_OpenList);
	// Clears the open nodes list
	m_OpenList.clear();
	// Frees nodes in the closed nodes list
	FreeUnusedNodesInVector(m_ClosedList);
	// Clears the closed nodes list
	m_ClosedList.clear();
}
void PlannerTools::FreeAllNodes()
{
	// iterate open list and delete all nodes
	FreeNodesInVector(m_OpenList);
	m_OpenList.clear();
	FreeNodesInVector(m_ClosedList);
	m_ClosedList.clear();
	delete m_Goal;
	//delete m_Start;
	delete m_Start;
}

bool PlannerTools::AddSuccessor(Node &Succesor)
{
	Node *node = new Node(Succesor);

	if( node )
	{
		m_Successors.push_back( node );

		return true;
	}

	return false;
}
void PlannerTools::FreeSolutionNodes()
{
	Node *n = m_Start;

	if( m_Start->childNode )
	{
		do
		{
			Node *del = n;
			n = n->childNode;
			delete del;
			del = NULL;
		} while( n != m_Goal );
		delete n;  // Delete the goal
	}
	else
	{
		// if the start node is the solution we need to just delete the start and goal
		// nodes
		delete m_Start;
		delete m_Goal;
	}
}

Node *PlannerTools::GetSolutionStart()
{
	m_CurrentSolutionNode = m_Start;
	if( m_Start )
	{
		return m_Start;
	}
	else
	{
		return NULL;
	}
}

Node *PlannerTools::GetSolutionNext()
{
	if( m_CurrentSolutionNode )
	{
		if( m_CurrentSolutionNode->childNode )
		{
			Node *child = m_CurrentSolutionNode->childNode;
			m_CurrentSolutionNode = m_CurrentSolutionNode->childNode;
			return child;
		}
	}

	return NULL;
}
Node *PlannerTools::GetSolutionEnd()
{
	m_CurrentSolutionNode = m_Goal;
	if( m_Goal )
	{
		return m_Goal;
	}
	else
	{
		return NULL;
	}
}

Node *PlannerTools::GetSolutionPrev()
{
	if( m_CurrentSolutionNode )
	{
		if( m_CurrentSolutionNode->parentNode )
		{

			Node *parent = m_CurrentSolutionNode->parentNode;

			m_CurrentSolutionNode = m_CurrentSolutionNode->parentNode;

			return parent;
		}
	}

	return NULL;
}


Node *PlannerTools::GetOpenListStart()
{
	double f,g,h;
	return GetOpenListStart( f,g,h );
}
Node *PlannerTools::GetOpenListStart( double &f, double &g, double &h )
{
	iterDbgOpen = m_OpenList.begin();
	if( iterDbgOpen != m_OpenList.end() )
	{
		f = (*iterDbgOpen)->f_score;
		g = (*iterDbgOpen)->g_score;
		h = (*iterDbgOpen)->h_score;
		return (*iterDbgOpen);
	}
	return NULL;
}

Node *PlannerTools::GetOpenListNext()
{
	double f,g,h;
	return GetOpenListNext( f,g,h );
}

Node *PlannerTools::GetOpenListNext( double &f, double &g, double &h )
{
	iterDbgOpen++;
	if( iterDbgOpen != m_OpenList.end() )
	{
		f = (*iterDbgOpen)->f_score;
		g = (*iterDbgOpen)->g_score;
		h = (*iterDbgOpen)->h_score;
		return (*iterDbgOpen);
	}

	return NULL;
}
Node *PlannerTools::GetClosedListStart()
{
	double f,g,h;
	return GetClosedListStart( f,g,h );
}

Node *PlannerTools::GetClosedListStart( double &f, double &g, double &h )
{
	iterDbgClosed = m_ClosedList.begin();
	if( iterDbgClosed != m_ClosedList.end() )
	{
		f = (*iterDbgClosed)->f_score;
		g = (*iterDbgClosed)->g_score;
		h = (*iterDbgClosed)->h_score;

		return (*iterDbgClosed);
	}

	return NULL;
}

Node *PlannerTools::GetClosedListNext()
{
	double f,g,h;
	return GetClosedListNext( f,g,h );
}

Node *PlannerTools::GetClosedListNext( double &f, double &g, double &h )
{
	iterDbgClosed++;
	if( iterDbgClosed != m_ClosedList.end() )
	{
		f = (*iterDbgClosed)->f_score;
		g = (*iterDbgClosed)->g_score;
		h = (*iterDbgClosed)->h_score;

		return (*iterDbgClosed);
	}

	return NULL;
}
bool PlannerTools::isNodeInMap(Node &node)
{
	return isNodeInMap(node.getPosX(),node.getPosY());
}
bool PlannerTools::isNodeInMap(int PosX, int PosY)
{
	return (PosX >= 0) && (PosY >= 0) && (PosX < (int)(costMap_.info.width)) && (PosY < (int)(costMap_.info.height));
	// return (PosX >= 0) && (PosY >= 0) && (PosX < 100) && (PosY < 100);
}
